.. _xci:

.. This work is licensed under a Creative Commons Attribution 4.0 International License.
.. SPDX-License-Identifier: CC-BY-4.0
.. (c) Open Platform for NFV Project, Inc. and its contributors

============================================
Cross Community Continuous Integration - XCI
============================================

XCI is not an Anuket Infrastructure project.

However, some details about XCI are in the chapters below.

- :ref:`XCI Overview <xci-overview>`
- :ref:`XCI Sandbox and User Guide <xci-user-guide>`

